# xptool

#### 介绍

P5 激活ADB工具

使用于G3i、P5 3.5(没有实测过)、3.6.1版本获取adb权限，前提是已经安装了termux软件，其他车型请自测。

#### 安装教程

1. 在termux中按顺序执行
   
   ```bash
   pkg update -y
   ```
   
   ```bash
   pkg install android-tools -y
   ```
   
   ```bash
   curl https://gitee.com/yzf_Air/xptool/raw/master/xptool -o xptool
   ```

2. 加权限
   
   ```bash
   chmod 777 xptool
   ```

#### 使用说明

3. 运行
   
   ```bash
   ./xptool
   ```
4. 安装鹏友工具箱
xptool开启adb后，安装鹏友工具箱，以后直接在工具箱就能开启adb了
XPeng-1.0.3-3-20240131-d55-release_103-d55_jiagu_sign.apk
鹏友工具箱需要开启权限，还需要安装权限狗或者appops软件


#### 常见问题

1.提示ADB安装失败

```bash
pkg install android-tools -y
```

在termux中手动安装一下，再重新运行xptool

2.其他问题

```bash
pkg update -y
pkg upgrade -y
```

或者按照提示执行相关指令后再重新安装一次

3.有效期已过
扫码通知作者更新

#### 打赏作者

![image](https://gitee.com/yzf_Air/xptool/raw/master/donate.png)
